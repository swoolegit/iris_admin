const isProduction = process.env.NODE_ENV === 'production'
module.exports = {
    //lintOnSave:false,
    productionSourceMap: isProduction,
    // 開發時間建議不要開啟 postcss , 否則造成css修改困難
    css: {
        sourceMap: !isProduction,
        extract: isProduction,
    },

    pluginOptions: {
      'style-resources-loader': {
        preProcessor: 'stylus',
        patterns: [
            require('path').resolve(__dirname, './src/style/default.module.styl'),
        ]
      }
    },

    configureWebpack: {
        devServer: {
            watchOptions: {
                ignored: /node_modules/,
            },
            disableHostCheck: true,
            port:8585,
        },
    },

    runtimeCompiler: false
}
