export default {
    config: {
        'app_name': '斯里蘭卡礦場',
        'domain_name': 'demo.icrown.app',
		'ApiUrl': '/api',
        'ImgUrl': '/uploads/',
        'menu_type':{ 
            //分類 1:報表查看 , 2:會員管理  , 3:充提管裡 , 4: 訂單管理 , 5: 商品管理 ,  7: 資金紀錄  , 10系統管理
            "10"   : '系統管理',
			"2"   : '會員管理',
			"3"   : '充提管裡',
            "4"   : '訂單管理',
            "5"   : '商品管理',
            "7"   : '資金紀錄',
            "1"   : '報表查看',
        },
        'group':[
            {
              value: 1,
              label: '系統管理員'
            }, {
              value: 2,
              label: '客服人員'
            }, {
              value: 3,
              label: '財務人員'
            }, {
              value: 4,
              label: '其他'
            },
        ],

    }
}
