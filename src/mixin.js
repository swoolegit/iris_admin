import moment from 'moment'
export const globalMixin = {
    methods: {
        getRechargeStatus(item){
            switch(item.Status)
            {
                case 0:
                    return this.$t('msg61');
                case 1:
                    return this.$t('msg62');
                case 2:
                    return this.$t('msg63');
            }
        },
        getEnableStatus(item){
            switch(item.Enable)
            {
                case 1:
                    return this.$t('msg1');
                case 0:
                    return this.$t('msg2');
            }
        },
        getGroupStatus(item){
            switch(item.GroupID)
            {
                case 1:
                    return this.$t('msg3');
                case 2:
                    return this.$t('msg4');
                case 3:
                    return this.$t('msg5');
                case 4:
                    return this.$t('msg6');
            }
        },
        getCashStatus(item){
            switch(item.Status)
            {
                case 0:
                    return this.$t('CashStatus.0');
                case 1:
                    return this.$t('CashStatus.1');
                case 2:
                    return this.$t('CashStatus.2');
            }
        },
        getSEDate(date)
        {
            let FromTime=''
            let ToTime=''
            if(date)
            {
                FromTime= moment(date[0]).format("YYYY-MM-DD")
                ToTime= moment(date[1]).format("YYYY-MM-DD")
            }
            return {FromTime:FromTime,ToTime:ToTime}
        },
        go2page(url){
			this.$router.push({path: url, query: {}});
        },
        reload_page()
        {
            this.$router.go(0);
        },
        error_func(errID){
            switch(errID)
            {
                case 1:
                    this.$store.dispatch('clearUserInfo');
                    return;
                case 3:
                    //var reload_issue=setInterval(this.reload_page(),3000);
                    //clearInterval(reload_issue);
                    //setTimeout(this.reload_page(),30000);
                    return;
            }
        },
        error_handler(e)
        {
            if(e.func==1)
            {
                this.toast(e.msg);
                this.$router.push("/login");
                return;
            }
            this.toast(e.msg);
            // if(e.func)
            // {
            //     this.error_func(e.func);
            // }
            return;
        },
        toast(msg){
                this.$message(msg);
        },
        message(msg){
                this.$message(msg);
        },
        back(){
            this.$router.back()
        },
        getMomentYDate(time){
            var dateObj = new Date(time);
            var momentObj = moment(dateObj);
            return moment(momentObj).format("YYYY-MM-DD");
        },
        getMomentDate(time){
            var dateObj = new Date(time);
            var momentObj = moment(dateObj);
            return moment(momentObj).format("MM-DD");
        },
        getMomentTime(time){
            var dateObj = new Date(time);
            var momentObj = moment(dateObj);
            return moment(momentObj).format("HH:mm");
        },
        getMomentFullDate(time)
        {
			if(time=="")
			{
				return "--";
			}
            var dateObj = new Date(time);
            var momentObj = moment(dateObj);
            return moment(momentObj).format("YYYY-MM-DD HH:mm");
        },
        getBidStatus(item){
            switch(item.Status)
            {
                case 0:
                    return this.$t('BidStatus.0');
                case 1:
                    return this.$t('BidStatus.1');
                case 2:
                    return this.$t('BidStatus.2');
            }
        },
        getWinerStatus(item){
            switch(item.Winer)
            {
                case 1:
                    return this.$t('msg78');
            }
            return "";
        },
		gethms(time)
		{
			let msec = 0;
			let day = 0;
			let hr = 0;
			let min = 0;
			let sec = 0;
			msec = time;
			day = parseInt(msec / 60 / 60 / 24);
			hr = parseInt(msec  / 60 / 60 % 24);
			min = parseInt(msec / 60 % 60);
			sec = parseInt(msec % 60);
			return {
				day:day > 9 ? day : '0' + day,
				hr:hr > 9 ? hr : '0' + hr,
				min:min > 9 ? min : '0' + min,
				sec:sec > 9 ? sec : '0' + sec
			}
		},
    }
}